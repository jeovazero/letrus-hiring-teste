import React, {Component} from 'react';
import './App.css';

// ====================================================
// Considerando intervalo fechado na posição dos 'mistakes'
// ====================================================
export function makeTextWithMistakes(text, mistakes) {
  const listText = text.split('\n').map(el => el.trim());
  
  const makeEMtag = (child, can) => can ? <em>{child}</em> : child;
  const canSlice = (start, end) => end > start;

  const makeParagraphWithMistakes = (paragraph, {start, end}) => (
    [start, end+1, paragraph.length].reduce(
      // reducer
      (acc, cur) => ({
        ans: canSlice(acc.index, cur)
            ? [...acc.ans, makeEMtag(paragraph.slice(acc.index, cur), cur === end+1)]
            : acc.ans,
        index: cur
      }),
      // estado inicial
      { ans: [], index: 0 }
    ).ans
  );

  return mistakes.map(
    elem => makeParagraphWithMistakes(listText[elem.paragraph], elem)
  );
}

class App extends Component {
  renderParagraph = (paragraph, i) => {
    const isString = (el) => typeof el === 'string';
    return (
      <p key={i}>
        {
          paragraph
          .filter(o => o !== "")
          .map( 
            (el, j) => isString(el)
            ? <span key={'_' + j}>{el}</span>
            : React.cloneElement(el, { key: '_'+j, className: "wrong" })
          )
        }
      </p>
    );
  };

  render() {
    const text = `Meo texto está errado
                  Segundo palagrafo
                  Segundo palagrafo é verdade!
                  Meu texto está erado`;
                  
    const mistakes = [
      {
        start: 0,
        end: 2,
        paragraph: 0,
      },
      {
        start: 8,
        end: 16,
        paragraph: 1,
      },
      {
        start: 8,
        end: 16,
        paragraph: 2,
      },
      {
        start: 15,
        end: 20,
        paragraph: 3,
      }
    ];

    // ========================================================================
    // Sua tarefa é implementar a função que converte as variáveis `text` e
    // `mistakes` na variável `textWithMistakes`, para textos e erros
    // arbitrários. Você deve fazer isso na função
    // `makeTextWithMistakes(text, mistakes)` usando outras funções de ajuda se
    // achar necessário
    // ========================================================================

    const textWithMistakes = makeTextWithMistakes(text, mistakes);
    
    return (
      <div className="App">
      { textWithMistakes.map(this.renderParagraph) }
      </div>
    );
  }
}

export default App;
