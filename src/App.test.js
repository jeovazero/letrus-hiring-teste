import React from 'react';
import ReactDOM from 'react-dom';
import { makeTextWithMistakes } from './App';
import { isObject } from 'util';


const toBeTextWithMistakes = (text, mistakes, expectedAnswer) => {
    const answer = makeTextWithMistakes(text, mistakes);

    answer.forEach((list, li) => {
        const expectedList = expectedAnswer[li];

        list.forEach( (element, ei) => {
            const expectedElement = expectedList[ei];
            
            // in case JSX element
            if(isObject(element)){
                // is tag EM
                expect(element.type).toBe('em');

                // children is String
                const childrenElem = element.props.children;
                expect(typeof childrenElem).toBe('string');

                expect(childrenElem).toBe(expectedElement.props.children);
            }else{
                expect(typeof element).toBe('string');
                expect(element).toBe(expectedElement);
            }
        })
    });
}

describe('Text with Mistakes', () => {
    it('Makers at the beginning', () => {
        const text = 'Meo texto está errado\nSedundo parágrafo\n';
        const mistakes = [
          {
            start: 0,
            end: 2,
            paragraph: 0,
          },
          {
            start: 0,
            end: 6,
            paragraph: 1,
          }
        ];

        const expectedAnswer = [
            [
              <em>Meo</em>,
              " texto está errado"
            ],
            [
              <em>Sedundo</em>,
              " parágrafo"
            ]
        ];

        toBeTextWithMistakes(text, mistakes, expectedAnswer);
    });

    it('Makers in the middle', () => {
        const text = 'Meu textu está errado\nSegundo parágrafo do txto inicial';
        
        const mistakes = [
          {
            start: 4,
            end: 8,
            paragraph: 0,
          },
          {
            start: 21,
            end: 24,
            paragraph: 1,
          }
        ];

        const expectedAnswer = [
            [
              "Meu ",
              <em>textu</em>,
              " está errado"
            ],
            [
              "Segundo parágrafo do ",
              <em>txto</em>,
              " inicial"
            ]
        ];

        toBeTextWithMistakes(text, mistakes, expectedAnswer)
    });

    it('Makers at the end', () => {
        const text = 'Meu texto está erado\nSegundo parágrafo do texto incial';
        const mistakes = [
          {
            start: 27,
            end: 32,
            paragraph: 1,
          },
          {
            start: 15,
            end: 20,
            paragraph: 0,
          }
        ];
        
        const expectedAnswer = [
            [
              "Segundo parágrafo do texto ",
              <em>incial</em>
            ],
            [
              "Meu texto está ",
              <em>erado</em>
            ]
        ];

        toBeTextWithMistakes(text, mistakes, expectedAnswer)
    });
})

